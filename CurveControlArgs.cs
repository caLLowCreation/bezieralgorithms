﻿#region Author
/*
     
     Jones St. Lewis Cropper (caLLow)
     
     Another caLLowCreation
     
     Visit us on Google+ and other social media outlets @caLLowCreation
     
     Thanks for using our product.
     
     Send questions/comments/concerns/requests to 
      e-mail: caLLowCreation@gmail.com
      subject: BezierAlgorithms
     
*/
#endregion

using BezierAlgorithms.Extensions;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace BezierAlgorithms
{
    /// <summary>
    /// Arguments passed to curve path calculator
    /// </summary>
    public class CurveControlArgs
    {
        /// <summary>
        /// Waypoints to use to create curves
        /// </summary>
        public IEnumerable<IControlNode> points { get; private set; }

        /// <summary>
        /// Loop curve route
        /// </summary>
        public bool loop { get; private set; }

        IEnumerable<IControlNode> controlNodes { get; set; }

        /// <summary>
        /// Calculated control sections
        /// </summary>
        public IEnumerable<CurveControlSection> controlSections { get; private set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="points">Waypoints to use for path</param>
        /// <param name="loop">Looped or not</param>
        public CurveControlArgs(IEnumerable<IControlNode> points, bool loop)
        {
            this.loop = loop;
            this.points = points;
        }

        /// <summary>
        /// Compute points along control section
        /// </summary>
        public void ComputeControlPoints()
        {
            controlNodes = CurveControlArgs.ComputeControlPoints(points, loop);
        }

        /// <summary>
        /// Slice the sections for faster processing
        /// </summary>
        public void SliceSections()
        {
            controlSections = CurveControlArgs.SliceSections(controlNodes.Select(c => c as ControlNode), loop);
        }

        /// <summary>
        /// Compute and slice sections in one call
        /// </summary>
        /// <param name="curveControlArgs">Arguments to pass to the curve calculator</param>
        /// <returns>Calculated curve sections</returns>
        public static CurveControlSection[] GetComputedAndSlicedSections(CurveControlArgs curveControlArgs)
        {
            var controlNodes = CurveControlArgs.ComputeControlPoints(curveControlArgs.points, curveControlArgs.loop);
            var controlSections = CurveControlArgs.SliceSections(controlNodes.Select(c => c as ControlNode), curveControlArgs.loop);
            return controlSections.ToArray();
        }

        static IEnumerable<IControlNode> CalculateChunkControlPoint(IEnumerable<IControlNode> chunk, List<IControlNode> points)
        {
            var length = points.Count;
            List<IControlNode> controlPoints = new List<IControlNode>();
            IEnumerator<IControlNode> item = chunk.GetEnumerator();
            while (item.MoveNext())
            {
                int prevIndex = item.Current.index.PreviousIndex(length - 1);
                int curIndex = item.Current.index;
                int nextIndex = item.Current.index == length - 1 ? 0.NextIndex(length) : item.Current.index.NextIndex(length);

                var previous = points[prevIndex];
                var current = points[curIndex];
                var next = points[nextIndex];

                controlPoints.AddRange(CurveControlArgs.GetControlNodes(current.position, previous.position, next.position, item.Current));
            }
            return controlPoints;
        }

        static IEnumerable<IControlNode> GetControlNodes(Vector3 currentPosition, Vector3 previousPosition, Vector3 nextPosition, IControlNode controlNode)
        {
            Vector3 tangent = (nextPosition - previousPosition).normalized;
            var q0 = currentPosition - controlNode.scale * tangent * (currentPosition - previousPosition).magnitude;
            var q1 = currentPosition + controlNode.scale * tangent * (nextPosition - currentPosition).magnitude;
            return new[]
            {
                ControlNode.CreateNode(controlNode.index, q0, controlNode.rotation, controlNode.scale),
                ControlNode.CreateNode(controlNode.index, controlNode.position, controlNode.rotation, controlNode.scale),
                ControlNode.CreateNode(controlNode.index, q1, controlNode.rotation, controlNode.scale)
            };
        }

        /// <summary>
        /// Computes control points
        /// </summary>
        /// <param name="points">Waypoints to use for path</param>
        /// <param name="loop">Looped or not</param>
        /// <returns></returns>
        public static IEnumerable<IControlNode> ComputeControlPoints(IEnumerable<IControlNode> points, bool loop)
        {
            List<IControlNode> controlPoints = new List<IControlNode>();
            if (points.Count() == 0) return controlPoints;

            List<IControlNode> pts = points.ToList();

            if (loop)
            {
                pts.Add(pts[0]);
            }

            List<IControlNode>[] chunks = pts.ChunkBy(pts.Count / 1);

            controlPoints.Clear();
            for (int i = 0; i < chunks.Length; i++)
            {
                controlPoints.AddRange(CalculateChunkControlPoint(chunks[i], pts));
            }
            return controlPoints;
        }

        /// <summary>
        /// Slice control nodes into chuncks for faster processing
        /// </summary>
        /// <param name="controlPoints">Collection of control nodes</param>
        /// <param name="loop">Looped or not</param>
        /// <returns>Chuncks of Control Sections</returns>
        public static IEnumerable<CurveControlSection> SliceSections(IEnumerable<ControlNode> controlPoints, bool loop)
        {
            List<CurveControlSection> controlSections = new List<CurveControlSection>();
            var cp = controlPoints.ToArray();
            for (int i = 1; i < cp.Length - 3; i += 3)
            {
                controlSections.Add(new CurveControlSection(cp, i));
            }

            if (!loop)
            {
                SetNoLoopControlNode(controlSections[0].p0, controlSections[0].q1, controlSections[0].q0);
                var last = controlSections.Count - 1;
                SetNoLoopControlNode(controlSections[last].p1, controlSections[last].q0, controlSections[last].q1);
            }

            return controlSections;
        }

        static void SetNoLoopControlNode(ControlNode p0, ControlNode qGet, ControlNode qSet)
        {
            var opposite = -(p0.position - qGet.position) * p0.scale;
            var behind = (p0.rotation * -Vector3.forward * 100 * p0.scale);
            qSet.position = p0.position + behind + opposite;
        }
    }

}