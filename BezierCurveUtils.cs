﻿#region Author
/*
     
     Jones St. Lewis Cropper (caLLow)
     
     Another caLLowCreation
     
     Visit us on Google+ and other social media outlets @caLLowCreation
     
     Thanks for using our product.
     
     Send questions/comments/concerns/requests to 
      e-mail: caLLowCreation@gmail.com
      subject: BezierAlgorithms
     
*/
#endregion

using UnityEngine;
namespace BezierAlgorithms
{
    /// <summary>
    /// Utility methods to calculate curve values
    /// </summary>
    public static class BezierCurveUtils
    {
        /// <summary>
        /// Get position along quadratic curve 2D
        /// </summary>
        /// <param name="Start">Start position</param>
        /// <param name="Control">Control node</param>
        /// <param name="End">End position</param>
        /// <param name="t">Time interval</param>
        /// <returns>Poistion along curve set by time interval</returns>
        public static Vector2 QuadraticBezier2(Vector2 Start, Vector2 Control, Vector2 End, float t)
        {
            return (((1 - t) * (1 - t)) * Start) + (2 * t * (1 - t) * Control) + ((t * t) * End);
        }

        /// <summary>
        /// Get position along quadratic curve 3D
        /// </summary>
        /// <param name="Start">Start position</param>
        /// <param name="Control">Control node</param>
        /// <param name="End">End position</param>
        /// <param name="t">Time interval</param>
        /// <returns>Poistion along curve set by time interval</returns>
        public static Vector3 QuadraticBezier3(Vector3 Start, Vector3 Control, Vector3 End, float t)
        {
            return (((1 - t) * (1 - t)) * Start) + (2 * t * (1 - t) * Control) + ((t * t) * End);
        }

        /// <summary>
        /// Get position along cubic curve 2D
        /// </summary>
        /// <param name="s">Start position</param>
        /// <param name="st">Start control node</param>
        /// <param name="et">End control node</param>
        /// <param name="e">End position</param>
        /// <param name="t">Time interval</param>
        /// <returns>Poistion along curve set by time interval</returns>
        public static Vector2 CubicBezier2(Vector2 s, Vector2 st, Vector2 et, Vector2 e, float t)
        {
            return (((-s + 3 * (st - et) + e) * t + (3 * (s + et) - 6 * st)) * t + 3 * (st - s)) * t + s;
        }

        /// <summary>
        /// Get position along cubic curve 3D
        /// </summary>
        /// <param name="s">Start position</param>
        /// <param name="st">Start control node</param>
        /// <param name="et">End control node</param>
        /// <param name="e">End position</param>
        /// <param name="t">Time interval</param>
        /// <returns>Poistion along curve set by time interval</returns>
        public static Vector3 CubicBezier3(Vector3 s, Vector3 st, Vector3 et, Vector3 e, float t)
        {
            return (((-s + 3 * (st - et) + e) * t + (3 * (s + et) - 6 * st)) * t + 3 * (st - s)) * t + s;
        }
    }

}