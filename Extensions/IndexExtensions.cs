﻿#region Author
/*
     
     Jones St. Lewis Cropper (caLLow)
     
     Another caLLowCreation
     
     Visit us on Google+ and other social media outlets @caLLowCreation
     
     Thanks for using our product.
     
     Send questions/comments/concerns/requests to 
      e-mail: caLLowCreation@gmail.com
      subject: BezierAlgorithms
     
*/
#endregion

namespace BezierAlgorithms.Extensions
{
    /// <summary>
    /// Extensions to provide index accessed collections
    /// </summary>
    public static class IndexExtensions
    {
        /// <summary>
        /// Previous index wrapping at zero to length
        /// </summary>
        /// <param name="index">Current index value</param>
        /// <param name="length">Maximum length exclusive</param>
        /// <returns>Index after change</returns>
        public static int PreviousIndex(this int index, int length)
        {
            return index > 0 ? index - 1 : length - 1;
        }

        /// <summary>
        /// Next index wrapping at length to zero
        /// </summary>
        /// <param name="index">Current index value</param>
        /// <param name="length">Maximum length exclusive</param>
        /// <returns>Index after change</returns>
        public static int NextIndex(this int index, int length)
        {
            return index < length - 1 ? index + 1 : 0;
        }

        /// <summary>
        /// Wraps the value of an index
        /// </summary>
        /// <param name="index">Current index value</param>
        /// <param name="changeValue">Amount to change by</param>
        /// <param name="length">Maximum length exclusive</param>
        /// <returns>Index after change</returns>
        public static int WrapIndex(this int index, int changeValue, int length)
        {
            if (changeValue < 0)
            {
                return index.PreviousIndex(length);
            }
            else if (changeValue > 0)
            {
                return index.NextIndex(length);
            }
            else
            {
                return index;
            }
        }
    }

}