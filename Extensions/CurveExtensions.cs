﻿#region Author
/*
     
     Jones St. Lewis Cropper (caLLow)
     
     Another caLLowCreation
     
     Visit us on Google+ and other social media outlets @caLLowCreation
     
     Thanks for using our product.
     
     Send questions/comments/concerns/requests to 
      e-mail: caLLowCreation@gmail.com
      subject: BezierAlgorithms
     
*/
#endregion

using System.Collections.Generic;
using UnityEngine;

namespace BezierAlgorithms.Extensions
{
    /// <summary>
    /// Extensions to support Curve nodes
    /// </summary>
    public static class CurveExtensions
    {

        /// <summary>
        /// Vector3 position matches waypoint position
        /// </summary>
        /// <typeparam name="T">Of IPathNode interface type</typeparam>
        /// <param name="waypoint">Waypoint to check</param>
        /// <param name="position">Position to check against</param>
        /// <returns>True if matched False if not</returns>
        public static bool IsMatchingPosition<T>(this T waypoint, Vector3 position) where T : IPathNode
        {
            return waypoint != null && waypoint.position == position;
        }

        /// <summary>
        /// Location matches waypoint position and rotation
        /// </summary>
        /// <typeparam name="T">Of IPathNode interface type</typeparam>
        /// <param name="waypoint">Waypoint to check</param>
        /// <param name="position">Position to check against</param>
        /// <param name="rotation">Rotation to check against</param>
        /// <returns>True if matched False if not</returns>
        public static bool IsMatching<T>(this T waypoint, Vector3 position, Quaternion rotation) where T : IPathNode
        {
            return waypoint.position == position && waypoint.rotation == rotation;
        }

        /// <summary>
        /// Find waypoint in list of waypoints
        /// </summary>
        /// <typeparam name="T">Of IPathNode interface type</typeparam>
        /// <param name="waypoint">Waypoint to search for</param>
        /// <param name="waypoints">Collection of waypoints</param>
        /// <returns>IPathNode if found or NullPathNode</returns>
        public static IPathNode FindIn<T>(this T waypoint, IEnumerable<T> waypoints) where T : IPathNode
        {
            return waypoints.FindAtMatchingPosition(waypoint);
        }

        /// <summary>
        /// Find waypoint at matching position
        /// </summary>
        /// <typeparam name="T">Of IPathNode interface type</typeparam>
        /// <param name="waypoints">Collection of waypoints</param>
        /// <param name="position">Position to check against</param>
        /// <returns>IPathNode if found or NullPathNode</returns>
        public static IPathNode FindAtMatchingPosition<T>(this IEnumerable<T> waypoints, Vector3 position) where T : IPathNode
        {
            IEnumerator<T> item = waypoints.GetEnumerator();
            while (item.MoveNext())
            {
                if (CurveExtensions.IsMatchingPosition(item.Current, position))
                {
                    return item.Current;
                }
            }
            return NullPathNode.Value;
        }

        /// <summary>
        /// Find waypoint at matching position
        /// </summary>
        /// <typeparam name="T">Of IPathNode interface type</typeparam>
        /// <param name="waypoints">Collection of waypoints</param>
        /// <param name="waypoint">Waypoint to search for</param>
        /// <returns>IPathNode if found or NullPathNode</returns>
        public static IPathNode FindAtMatchingPosition<T>(this IEnumerable<T> waypoints, T waypoint) where T : IPathNode
        {
            return waypoints.FindAtMatchingPosition<T>(waypoint.position);
        }

        /// <summary>
        /// Find waypoint at least distance
        /// </summary>
        /// <typeparam name="T">Of IPathNode interface type</typeparam>
        /// <param name="waypoints">Collection of waypoints</param>
        /// <param name="position">Position to check against</param>
        /// <returns>IPathNode if found or NullPathNode</returns>
        public static IPathNode FindAtLeastDistance<T>(this IEnumerable<T> waypoints, Vector3 position) where T : IPathNode
        {
            IPathNode returnWaypoint = NullPathNode.Value;
            float minDistance = float.MaxValue;
            IEnumerator<T> waypoint = waypoints.GetEnumerator();
            while (waypoint.MoveNext())
            {
                float dist = Vector3.Distance(waypoint.Current.position, position);
                if (minDistance > dist)
                {
                    minDistance = dist;
                    returnWaypoint = waypoint.Current;
                }
            }
            return returnWaypoint;
        }

        /// <summary>
        /// Find waypoint at least distance in collection
        /// </summary>
        /// <typeparam name="T">Of IPathNode interface type</typeparam>
        /// <param name="waypoint">Waypoint to search for</param>
        /// <param name="waypoints">Collection of waypoints</param>
        /// <returns>IPathNode if found or NullPathNode</returns>
        public static IPathNode FindInAtLeastDistance<T>(this T waypoint, IEnumerable<T> waypoints) where T : IPathNode
        {
            return waypoints.FindAtLeastDistance(waypoint.position);
        }

    }
}