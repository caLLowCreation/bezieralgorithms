﻿#region Author
/*
     
     Jones St. Lewis Cropper (caLLow)
     
     Another caLLowCreation
     
     Visit us on Google+ and other social media outlets @caLLowCreation
     
     Thanks for using our product.
     
     Send questions/comments/concerns/requests to 
      e-mail: caLLowCreation@gmail.com
      subject: BezierAlgorithms     
     
*/
#endregion

namespace BezierAlgorithms
{
    /// <summary>
    /// Control node interface used to calculate curve scale and trejectory
    /// </summary>
    public interface IControlNode : IPathNode
    {

        /// <summary>
        /// Scale of distance of curve
        /// </summary>
        float scale { get; set; }

    }

}