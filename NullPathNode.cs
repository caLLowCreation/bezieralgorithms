﻿#region Author
/*
     
     Jones St. Lewis Cropper (caLLow)
     
     Another caLLowCreation
     
     Visit us on Google+ and other social media outlets @caLLowCreation
     
     Thanks for using our product.
     
     Send questions/comments/concerns/requests to 
      e-mail: caLLowCreation@gmail.com
      subject: BezierAlgorithms
     
*/
#endregion

using UnityEngine;

namespace BezierAlgorithms
{
    //06-07-2015
    /// <summary>
    /// Null IPathNode interface object to use when PathPoint is not set.
    /// </summary>
    [System.Serializable]
    internal sealed class NullPathPoint : PathPoint
    {
        #region Static Fields

        static IPathPoint m_Value;

        #endregion

        #region Static Accessors

        /// <summary>
        /// Default null path node value
        /// </summary>
        public readonly static IPathPoint Value = m_Value ?? (m_Value = new NullPathPoint());

        #endregion

        #region Constructors

        NullPathPoint()
            : base(PathPoint.DEFAULT_INDEX, Vector3.zero)
        { }

        #endregion

        #region IPathNode Implementation

        /// <summary>
        /// Null path point string value
        /// </summary>
        /// <returns>Representation of NullPathPoint</returns>
        public override string ToString() { return "Null IPathPoint"; }

        #endregion
    }

}
