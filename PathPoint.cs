﻿#region Author
/*
     
     Jones St. Lewis Cropper (caLLow)
     
     Another caLLowCreation
     
     Visit us on Google+ and other social media outlets @caLLowCreation
     
     Thanks for using our product.
     
     Send questions/comments/concerns/requests to 
      e-mail: caLLowCreation@gmail.com
      subject: BezierAlgorithms
     
*/
#endregion

using UnityEngine;

namespace BezierAlgorithms
{
    //07-04-2015
    /// <summary>
    /// The path point object
    /// </summary>
    [System.Serializable]
    public class PathPoint : IPathPoint
    {
        /// <summary>
        /// Default index set to int.MinValue
        /// </summary>
        public const int DEFAULT_INDEX = int.MinValue;

        public static readonly IPathPoint Empty = NullPathPoint.Value;

        [SerializeField]
        int m_Index = DEFAULT_INDEX;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2235:MarkAllNonSerializableFields")]
        [SerializeField]
        Vector3 m_Position;

        /// <summary>
        /// Index of along path
        /// </summary>
        public int index
        {
            get { return m_Index; }
            set { m_Index = value; }
        }

        /// <summary>
        /// 3D position
        /// </summary>
        public Vector3 position
        {
            get { return m_Position; }
            set { m_Position = value; }
        }
        
        /// <summary>
        /// Constructor
        /// </summary>
        public PathPoint()
        { }

        /// <summary>
        /// Constructor to set values
        /// </summary>
        /// <param name="index">Index along path</param>
        /// <param name="position">3D position</param>
        protected PathPoint(int index, Vector3 position)
        {
            this.m_Index = index;
            this.m_Position = position;
        }

        /// <summary>
        /// Create a new Path Point
        /// </summary>
        /// <param name="index">Index along path</param>
        /// <param name="position">3D position</param>
        /// <returns>New Path Point</returns>
        public static IPathPoint CreatePoint(int index, Vector3 position)
        {
            var pathPoint = new PathPoint(index, position);
            return pathPoint;
        }

        /// <summary>
        /// Shows comprehensive representation of a path Point
        /// </summary>
        /// <returns>PathPoint representation</returns>
        public override string ToString()
        {
            return string.Format("Index[{0}]\nPosition:{1}\n", index, position);
        }

        /// <summary>
        /// Defines an empty or null value PathPoint
        /// <para>Usually PathPoint.DEFAULT_INDEX will be the null or empty condition.</para>
        /// </summary>
        /// <param name="pathPoint">Path Point to evaluate</param>
        /// <returns>True if value equals null or PathPoint.DEFAULT_INDEX or false it index is valid</returns>
        public static bool IsEmpty(IPathPoint pathPoint)
        {
            return pathPoint == null || pathPoint.index.Equals(PathPoint.DEFAULT_INDEX);
        } 

        /// <summary>
        /// Defines an empty or null value PathPoint
        /// <para>Usually PathPoint.DEFAULT_INDEX will be the null or empty condition.</para>
        /// </summary>
        /// <returns>True if value equals PathPoint.DEFAULT_INDEX or false it index is valid</returns>
        public bool IsEmpty()
        {
            return PathPoint.IsEmpty(this);
        }
    }
}
