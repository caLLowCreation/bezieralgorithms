﻿#region Author
/*
     
     Jones St. Lewis Cropper (caLLow)
     
     Another caLLowCreation
     
     Visit us on Google+ and other social media outlets @caLLowCreation
     
     Thanks for using our product.
     
     Send questions/comments/concerns/requests to 
      e-mail: caLLowCreation@gmail.com
      subject: BezierAlgorithms     
     
*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace BezierAlgorithms
{
    /// <summary>
    /// Curve section of points to control section curve
    /// </summary>
    [System.Serializable]
    public class CurveControlSection
    {
        /// <summary>
        /// Default amount of segments to use
        /// </summary>
        public const int DISTANCE_MEASURE_SEGMENTS = 20;
        [SerializeField]
        ControlNode m_P0;
        [SerializeField]
        ControlNode m_P1;
        [SerializeField]
        ControlNode m_Q0;
        [SerializeField]
        ControlNode m_Q1;

        [SerializeField]
        PathPoint[] m_PathPoints = null;

        /// <summary>
        /// Control nodes in an array
        /// </summary>
        public ControlNode[] ControlNodes { get { return new[] { m_P0, m_Q0, m_Q1, m_P1 }; } }

        /// <summary>
        /// First control node
        /// </summary>
        public ControlNode p0 { get { return m_P0; } }

        /// <summary>
        /// Last control node
        /// </summary>
        public ControlNode p1 { get { return m_P1; } }

        /// <summary>
        /// First control point
        /// </summary>
        public ControlNode q0 { get { return m_Q0; } set { m_Q0 = value; } }

        /// <summary>
        /// Last control point
        /// </summary>
        public ControlNode q1 { get { return m_Q1; } set { m_Q1 = value; } }

        /// <summary>
        /// Last control point
        /// </summary>
        public PathPoint[] PathPoints { get { return m_PathPoints; } set { m_PathPoints = value; } }

        [SerializeField]
        float m_Distance;

        /// <summary>
        /// Distance of this control section calculated by using the amount of segments
        /// </summary>
        public float distance { get { return m_Distance; } }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="controlPoints">Control points to use for curve sections</param>
        /// <param name="sectionIndex">Index to start this section</param>
        public CurveControlSection(ControlNode[] controlPoints, int sectionIndex)
        {
            m_P0 = controlPoints[sectionIndex];
            m_Q0 = controlPoints[sectionIndex + 1];
            m_Q1 = controlPoints[sectionIndex + 2];
            m_P1 = controlPoints[sectionIndex + 3];
            this.CalculateDistance(CurveControlSection.DISTANCE_MEASURE_SEGMENTS);
            this.CalculatePathPoints(CurveControlSection.DISTANCE_MEASURE_SEGMENTS);
        }

        /// <summary>
        /// Calculate section curve
        /// </summary>
        /// <param name="segments">How many segments to use to calculate curve</param>
        /// <returns>Collection of path nodes representing the curve</returns>
        public IEnumerable<IPathNode> CalculateCurve(int segments)
        {
            return CurveControlSection.CalculateCurve(segments, ControlNodes, 0);
        }

        /// <summary>
        /// Calculate curve distance
        /// </summary>
        /// <param name="segments">How many segments to use to calculate distance</param>
        public void CalculateDistance(int segments)
        {
            m_Distance = CalculateDistance(segments, ControlNodes, 0);
        }

        public void CalculatePathPoints(int segments)
        {
            m_PathPoints = CalculateCurve(segments)
                .Select(x => PathPoint.CreatePoint(x.index, x.position) as PathPoint)
                .ToArray();
        }

        /// <summary>
        /// Calculate curve distance
        /// </summary>
        /// <param name="segments">How many segments to use to calculate distance</param>
        /// <param name="controlPoints">Control points of curves</param>
        /// <param name="index">Section index</param>
        /// <returns>Distance of curve</returns>
        public static float CalculateDistance(int segments,IControlNode[] controlPoints, int index)
        {
            var linkedNodes = CurveControlSection.CalculateCurve(segments, controlPoints, index).ToArray();
            var dist = 0.0f;
            for (int i = 0; i < linkedNodes.Length - 1; i++)
            {
                var node = linkedNodes[i];
                var next = linkedNodes[i + 1];
                dist += Vector3.Distance(node.position, next.position);
            }
            return dist;
        }

        /// <summary>
        /// Calculates curve
        /// </summary>
        /// <param name="segments">How many segments to use to calculate curve</param>
        /// <param name="controlNodes">Control points to use for curve sections</param>
        /// <param name="startIndex">Index to start this section</param>
        /// <returns>Collection of path nodes representing the curve</returns>
        public static IEnumerable<IPathNode> CalculateCurve(int segments, IControlNode[] controlNodes, int startIndex)
        {
            var p0 = controlNodes[startIndex];
            var p1 = controlNodes[startIndex + 1];
            var p2 = controlNodes[startIndex + 2];
            var p3 = controlNodes[startIndex + 3];

            return CalculateCurveSegments(segments, p0, p1, p2, p3);
        }

        static IPathNode[] CalculateCurveSegments(int segments, IPathNode c0, IPathNode c1, IPathNode c2, IPathNode c3)
        {
            IPathNode[] PathPoints = new IPathNode[segments + 1];
            for (int j = 0; j <= segments; j++)
            {
                float t = j / (float)segments;
                float u = 1 - t;

                float Tpow2 = t * t;
                float Tpow3 = Tpow2 * t;

                float Upow2 = u * u;
                float Upow3 = Upow2 * u;
                var vec3 = (Upow3 * c0.position) + (3 * Upow2 * t * c1.position) + (3 * u * Tpow2 * c2.position) + (Tpow3 * c3.position);
                PathPoints[j] = PathNode.CreateNode(segments * c0.index + j, vec3, c0.rotation);
            }
            return PathPoints;
        }
    }
}