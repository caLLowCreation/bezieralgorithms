﻿#region Author
/*
     
     Jones St. Lewis Cropper (caLLow)
     
     Another caLLowCreation
     
     Visit us on Google+ and other social media outlets @caLLowCreation
     
     Thanks for using our product.
     
     Send questions/comments/concerns/requests to 
      e-mail: caLLowCreation@gmail.com
      subject: BezierAlgorithms
     
*/
#endregion

using UnityEngine;

namespace BezierAlgorithms
{
    //07-04-2015
    /// <summary>
    /// The path node object
    /// </summary>
    [System.Serializable]
    public class PathNode : PathPoint, IPathNode
    {
        /// <summary>
        /// Default index set to int.MinValue
        /// </summary>
        public const int DEFAULT_INDEX = PathPoint.DEFAULT_INDEX;

        public static readonly IPathNode Empty = NullPathNode.Value;

        /// <summary>
        /// Checks if look at transform is in line of sight
        /// </summary>
        /// <param name="position">Position of Path Node</param>
        /// <param name="lookAt">Transform to look at</param>
        /// <param name="minDistance">Minimum distance to consider</param>
        /// <returns></returns>
        public static bool CheckInLoS(Vector3 position, Transform lookAt, ref float minDistance)
        {
            //Set direction to m_LookAt object
            Vector3 direction = lookAt.position - position;
            //Get distance between the origin and direction
            float distance = Vector3.Distance(position, direction);

            //Raycast to m_LookAt object plus 1.2% distance
            RaycastHit hitInfo;
            if (Physics.Raycast(position, direction, out hitInfo, distance * 1.2f))
            {
                //Check that rayhit our m_LookAt object
                if (hitInfo.collider.transform == lookAt)
                {
                    //If hit distance is less that min distance
                    if (hitInfo.distance < minDistance)
                    {
                        //Set min distance to hit distance as closest distance
                        minDistance = hitInfo.distance;
                        return true;
                    }
                }
            }
            return false;
        }
        
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2235:MarkAllNonSerializableFields")]
        [SerializeField]
        Quaternion m_Rotation;
        
        /// <summary>
        /// Orientation of path node
        /// </summary>
        public Quaternion rotation
        {
            get { return m_Rotation; }
            set { m_Rotation = value; }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public PathNode()
        { }

        /// <summary>
        /// Constructor to set values
        /// </summary>
        /// <param name="index">Index along path</param>
        /// <param name="position">3D position</param>
        /// <param name="rotation">Orientation of path node</param>
        protected PathNode(int index, Vector3 position, Quaternion rotation)
            : base(index, position)
        {
            this.m_Rotation = rotation;
        }

        /// <summary>
        /// Create a new Path Node
        /// </summary>
        /// <param name="index">Index along path</param>
        /// <param name="position">3D position</param>
        /// <param name="rotation">Orientation of path node</param>
        /// <returns>New Path Node</returns>
        public static IPathNode CreateNode(int index, Vector3 position, Quaternion rotation)
        {
            var pathNode = new PathNode(index, position, rotation);
            return pathNode;
        }

        /// <summary>
        /// Shows comprehensive representation of a path node
        /// </summary>
        /// <returns>PathNode representation</returns>
        public override string ToString()
        {
            return string.Format("Index[{0}]\nPosition:{1}\nRotation:{2}", index, position, rotation);
        }

        /// <summary>
        /// Defines an empty or null value PathNode
        /// <para>Usually PathNode.DEFAULT_INDEX will be the null or empty condition.</para>
        /// </summary>
        /// <param name="pathNode">Path node to evaluate</param>
        /// <returns>True if value equals null or PathNode.DEFAULT_INDEX or false it index is valid</returns>
        public static bool IsEmpty(IPathNode pathNode)
        {
            return pathNode == null || pathNode.index.Equals(PathNode.DEFAULT_INDEX);
        } 

        /// <summary>
        /// Defines an empty or null value PathNode
        /// <para>Usually PathNode.DEFAULT_INDEX will be the null or empty condition.</para>
        /// </summary>
        /// <returns>True if value equals PathNode.DEFAULT_INDEX or false it index is valid</returns>
        public bool IsEmpty()
        {
            return PathNode.IsEmpty(this);
        }
    }
}
