﻿#region Author
/*
     
     Jones St. Lewis Cropper (caLLow)
     
     Another caLLowCreation
     
     Visit us on Google+ and other social media outlets @caLLowCreation
     
     Thanks for using our product.
     
     Send questions/comments/concerns/requests to 
      e-mail: caLLowCreation@gmail.com
      subject: BezierAlgorithms
     
*/
#endregion

using UnityEngine;

namespace BezierAlgorithms
{
    //07-04-2015
    /// <summary>
    /// The path node object
    /// </summary>
    [System.Serializable]
    public class ControlNode : PathNode, IControlNode
    {
        [SerializeField]
        float m_Scale;

        /// <summary>
        /// Scale of curve
        /// </summary>
        public float scale
        {
            get { return m_Scale; }
            set { m_Scale = value; }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="segment">Segment index</param>
        /// <param name="position">3D position</param>
        /// <param name="rotation">Orientation of control node</param>
        /// <param name="scale">Control node scale</param>
        protected ControlNode(int segment, Vector3 position, Quaternion rotation, float scale)
            : base(segment, position, rotation)
        {
            this.m_Scale = scale;
        }

        /// <summary>
        /// Creates new Control Node
        /// </summary>
        /// <param name="segment">Segment index</param>
        /// <param name="position">3D position</param>
        /// <param name="rotation">Orientation of control node</param>
        /// <param name="scale">Control node scale</param>
        /// <returns>New Control Node</returns>
        public static IControlNode CreateNode(int segment, Vector3 position, Quaternion rotation, float scale)
        {
            var controlNode = new ControlNode(segment, position, rotation, scale);
            return controlNode;
        }

        /// <summary>
        /// Shows comprehensive representation of a control node
        /// </summary>
        /// <returns>ControlNode representation</returns>
        public override string ToString()
        {
            return string.Format("Scale: {0}\n{1}", scale, base.ToString());
        }

    }
}
