﻿#region Author
/*
     
     Jones St. Lewis Cropper (caLLow)
     
     Another caLLowCreation
     
     Visit us on Google+ and other social media outlets @caLLowCreation
     
     Thanks for using our product.
     
     Send questions/comments/concerns/requests to 
      e-mail: caLLowCreation@gmail.com
      subject: BezierAlgorithms
     
*/
#endregion

using UnityEngine;

namespace BezierAlgorithms
{
    //06-07-2015
    /// <summary>
    /// Null IPathNode interface object to use when PathNode is not set.
    /// </summary>
    [System.Serializable]
    internal sealed class NullPathNode : PathNode
    {
        #region Static Fields

        static IPathNode m_Value;

        #endregion

        #region Static Accessors

        /// <summary>
        /// Default null path node value
        /// </summary>
        public readonly static IPathNode Value = m_Value ?? (m_Value = new NullPathNode());

        #endregion

        #region Constructors

        NullPathNode()
            : base(PathNode.DEFAULT_INDEX, Vector3.zero, Quaternion.identity)
        { }

        #endregion

        #region IPathNode Implementation

        /// <summary>
        /// Null path node string value
        /// </summary>
        /// <returns>Representation of NullPathNode</returns>
        public override string ToString() { return "Null IPathNode"; }

        #endregion
    }

}
