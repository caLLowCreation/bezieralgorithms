﻿#region Author
/*
     
     Jones St. Lewis Cropper (caLLow)
     
     Another caLLowCreation
     
     Visit us on Google+ and other social media outlets @caLLowCreation
     
     Thanks for using our product.
     
     Send questions/comments/concerns/requests to 
      e-mail: caLLowCreation@gmail.com
      subject: BezierAlgorithms     
     
*/
#endregion

using UnityEngine;

namespace BezierAlgorithms
{
    /// <summary>
    /// Generic node along path
    /// </summary>
    public interface IPathNode
    {
        /// <summary>
        /// Index along route
        /// </summary>
        int index { get; set; }

        /// <summary>
        /// Gets the current position of the PathNode
        /// </summary>
        Vector3 position { get; set; }

        /// <summary>
        /// Gets the current rotation of the PathNode
        /// </summary>
        Quaternion rotation { get; set; }

        /// <summary>
        /// Defines an empty or null value PathNode
        /// <para>Usually PathNode.DEFAULT_INDEX will be the null or empty condition.</para>
        /// </summary>
        /// <returns>True if value equals PathNode.DEFAULT_INDEX or false it index is valid</returns>
        bool IsEmpty();
    }

}